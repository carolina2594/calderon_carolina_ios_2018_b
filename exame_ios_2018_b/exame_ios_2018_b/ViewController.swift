//
//  ViewController.swift
//  exame_ios_2018_b
//
//  Created by Carolina Calderon on 27/11/18.
//  Copyright © 2018 Carolina Calderon. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var mailTextField: UITextField!
    
    @IBOutlet weak var paswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    

    @IBAction func loginPressedButton(_ sender: Any) {
        
        let username = mailTextField.text!
        let password = paswordTextField.text!
        
        Auth.auth().signIn(withEmail: username, password: password) { (data, error) in
            if let error = error{
                print(error)
                return
            }
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
        
    }
}

