//
//  numerosViewController.swift
//  exame_ios_2018_b
//
//  Created by Carolina Calderon on 27/11/18.
//  Copyright © 2018 Carolina Calderon. All rights reserved.
//

import UIKit

class numerosViewController: UIViewController {

    @IBOutlet weak var ingresarNumeroTextField: UITextField!
    @IBOutlet weak var resultadoTextField: UILabel!
     let numeros = Numeros()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func obtenerNumerosButtonPressed(_ sender: Any) {
        let numero = ingresarNumeroTextField.text!
        let entero = Int(numero)
        
        numeros.obtenerNumeros(entero ?? 0)
        
        let stringNumero = String (numeros.serieNumeros)
        
        resultadoTextField.text = stringNumero
        
    }
    
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    
}
